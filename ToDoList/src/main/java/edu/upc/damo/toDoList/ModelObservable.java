package edu.upc.damo.toDoList;

import java.util.Iterator;

/**
 * Created by Josep M on 24/10/2014.
 */
public class ModelObservable implements Iterable<CharSequence> {

    // Comportament d'observable

    private OnCanviModelListener observador;
    private Model model;

    public ModelObservable(Model model) {
        this.model = model;
    }

    public void setOnCanviModelListener(OnCanviModelListener observador) {
        this.observador = observador;
    }

    private void avisaObservador() {
        if (observador != null)
            observador.onNovesDades();
    }

    public void add(CharSequence s) {
        model.add(s);
        avisaObservador();
    }

    // Delegació al model embolcallat

    public void del(int pos) {
        model.del(pos);
        avisaObservador();
    }

    public int size() {
        return model.size();
    }

    public Object getItem(int position) {
        return model.getItem(position);
    }

    public long getItemId(int position) {
        return model.getItemId(position);
    }

    @Override
    public Iterator<CharSequence> iterator() {
        return model.iterator();
    }

    public interface OnCanviModelListener {
        public void onNovesDades();
    }


}

